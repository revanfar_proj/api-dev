import psutil, libvirt, os, time, json
from xml.etree import ElementTree

class Host():
	"""
	Class for getting needed statistic from vCPE (Host), like current CPU Usage, Memory usage, total Disk usage, and network usage (send, receive).

	Using psutil library to make easier getting detail usage statistic data, then filter it to get needed data and send to server.
	"""


	def host_cpu(self):
		i = 0
		data = {}
		#It's get realtime usage each cpu with 1 secs interval, return it as dictionary type
		cpu = psutil.cpu_percent(interval=1, percpu=True)
		for core in cpu:
			data["core"+str(i)] = core
			i+=1
		return data


	def host_ram(self):
		data = {}
		#get all detail memory usage, but only return percentage and maximum capacity as dictionary
		mem = psutil.virtual_memory()
		data['percent'] = mem.percent
		data['max'] = mem.total
		return data


	def host_disk(self):
		"""
		In psutil, current disk read/write statistic and disk statistic (used, max. capacity, etc.) not in same function. And disk statistic is total read or total write, to get current read/write statistic (not total), it need to set interval then calculate the difference.
		"""

		data = {}
		strg1 = psutil.disk_io_counters()
		time.sleep(1)
		strg2 = psutil.disk_io_counters()
		data['read'] = strg2.read_bytes-strg1.read_bytes
		data['write'] = strg2.write_bytes-strg2.write_bytes

		strg = psutil.disk_usage('/')
		data['usage'] = strg.used
		data['max'] = strg.total
		return data


	def host_net(self):
		"""
		It only getting sent & receive statistic from enp2s0 and enp3s0 interface, if device have difference name (because different card/linux distro) it need to set manually.
		Like disk usage, network data flow just show total bytes, packet, and error, it need to set interval then calculate the difference each interval.

		"""

		data = {}
		nt = psutil.net_io_counters(pernic=True)
		time.sleep(1)
		nt2 = psutil.net_io_counters(pernic=True)
		for nic in nt:
			if nic=='enp2s0' or nic=='enp3s0':
				data[nic] = {}
				sent = abs(nt2[nic].bytes_sent - nt[nic].bytes_sent)
				recv = abs(nt2[nic].bytes_recv - nt[nic].bytes_recv)
				data[nic]['sent'] = sent
				data[nic]['recv'] = recv
		return data


	def stat(self):
		#get all statistic from function above, then combine it as dict data with specific key each stats.
		data = {}
		data['cpu'] = self.host_cpu()
		data['ram'] = self.host_ram()
		data['disk'] = self.host_disk()
		data['net'] = self.host_net()
		return data



class VNF():
	def __init__(self, name):
		self.conn = libvirt.open('qemu:///system')
		self.dom = self.conn.lookupByName(name)


	def vnf_state(self):
		"""checking vnf is active or inactive.
		Return:
			int: 1 if domain is active, 0 if inactive
		"""
		return self.dom.isActive()

		
	def vnf_cpu(self):
		#get cpu allocation for current vnf
		return self.dom.info()[3]


	def vnf_ram(self):
		#get max memory allocation for current vnf
		return self.dom.info()[2]


	def vnf_disk(self):
		#get total disk usage for current vnf (qcow2 size)
		tree = ElementTree.fromstring(self.dom.XMLDesc())
		target = tree.find('devices/disk/source').get('file')
		res = os.path.getsize(target)
		return res


	def stat(self):
		#combine all stats, with given keys. Return as dict type
		data = {}
		data['state'] = self.vnf_state()
		data['cpu'] = self.vnf_cpu()
		data['ram'] = self.vnf_ram()
		data['disk'] = self.vnf_disk()
		return data

	def __del__(self):
		#close libvirt instance when class is closed or destroyed. To avoid error on next class instantiation
		self.conn.close()



class Stats():
	"""
	combining all stats (host/vcpe and each vnf) become one dict type. Add datetime with format, in example 20170409-1236 (9th april 2017, 12:36)
	"""
	def getStats(self):
		data = {}
		data['datetime']=time.strftime("%Y%m%d-%H%M")
		host = Host()
		data['host'] = host.stat()

		data['vRouter'] = False
		data['vFirewall'] = False

		try:
			vrouter = VNF('vRouter')
			data['vRouter'] = vrouter.stat()
			del vrouter
		except Exception as e:
			print e
		try:
			vfirewall = VNF('vFirewall')
			data['vFirewall'] = vfirewall.stat()
			del vfirewall
		except Exception as e:
			print e		

		return data

'''
stat = Stats()
print json.dumps(stat.getStats())
'''