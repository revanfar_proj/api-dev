from datetime import datetime
from stats import Stats
from conf_checker import ConfCheck
from apscheduler.schedulers.background import BackgroundScheduler
import time, os, urllib2, json, hashlib

"""Scheduled Synchronization & Statistic Sender Agent

It's contains method to send config synchronization and vCPE usage and statistic. It's automatic and scheduled every 2 minutes (for config sync) and 15 minutes (for usage and statistic).

Statistic Sender is real-time and doesn't keep any statistic if vCPE is disconnected from server.
"""


def print_log(msg):
    """Log Printing Standarization

    It's adding current date and time information while printing log for debugging.

    Args:
        msg (str) : log message

    Examples:
        >>> msg = "some message"
        >>> print_log(msg)
        [2017-04-05 21:24:36] some message

    """
    print '['+datetime.now().strftime('%Y-%m-%d %H:%M:%S')+']', msg



def req_config(vcpe_id):
    """Config Synchronization Module

    It read current version of configuration file (from 'conf_example.json.old' in 'data' folder) and send it using JSON format to server with POST method for hiding the data.

    If server return True, it's means configuration is same between server and vCPE, and ignoring any configuration checking and printing "version same"

    If server return False, it's means there are new configuration file in server, and vCPE will save new configuration to 'conf_example.json' in 'data' folder for next config compare.

    If module can't send config request (disconnected from server, or some reason), it will ignore the error. But not print log text (future development).

    Args:
        vcpe_id(str) : id of current vcpe

    """

    #ver : current version of vCPE Configuration file
    confdir = '../data/conf_example.json' 
    f = open(confdir+'.old', 'r')
    ver = json.loads(f.read())['version']
    f.close()

    print_log('sending config request (ver. '+str(ver)+')')

    #data : sending version using JSON format. For future development, adding security token in 'data' dictionary.
    data = {'version':ver, 'token':create_token(vcpe_id)}    
    #data = {'version':ver}

    """URL for config Synchronization
    for example, vcpe_id is '123456' it will send request to 'http://10.8.0.1/api/config/123456', and server will return with JSON format data
    """
    req = urllib2.Request('http://10.8.0.1/api/config/'+vcpe_id)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(data))
    
    #convert JSON to python dictionary
    res_arr = json.loads(response.read())

    if res_arr['result']==True:
        print_log('version same')

    #if result is false, res_arr will saved to conf_example.json file with JSON format, and call checkchange function to compare with conf_example.json.old
    if res_arr['result']==False:
        f = open(confdir, 'w')
        f.write(json.dumps(res_arr['conf']))
        f.close()
        cc = ConfCheck()
        res = cc.checkchange()    
        #any result from checkchange (config comment, config error, or some info) will printed to log text
        print_log(res)



def send_stat(vcpe_id):
    """Usage and Statistic Sender Module
    this module for send current statistic (date and time when this function is called) to server

    Args:
        vcpe_id(str) : id of current vcpe

    """

    #data : getting stats & usage summary of vcpe (type is dictionary)
    print_log('sending vCPE stats')
    s = Stats()
    data = s.getStats()

    #token : adding security token to 'data' dictionary
    data['token'] = create_token(vcpe_id)

    """URL for Statistic Receiver
    for example, vcpe_id is '123456' it will send request to 'http://10.8.0.1/api/statistic/123456', and server will return success result. And it use POST method to send 'data' payload in JSON format
    """

    req = urllib2.Request('http://10.8.0.1/api/statistic/'+vcpe_id)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(data))
    print_log(response.read())



def create_token(vcpe_id):
    """Token generator

    This function generate token with below algorithm:
        1. datetime : getting current datetime and rounding the seconds to nearest tens        
        2. vcpe_id : id of current vcpe
        3. datetime will formated to, for example, '2017-4-5_7:8:20'
        4. datetime and vcpe_id will combined become '2017-4-5_7:08:20|123456' (123456 is current vcpe_id)
        5. and then hashing this combination using md5 to get the token.

    datetime will have dynamic length because we remove zero leading for number under 10 (month, days, and hours), to make md5 result more varied and hardening for cracking

    Server have same algorithm too, and give 10 seconds tolerance for connection delay to keep token same between vCPE and server.

    Example:
        for example, current date is April 5th 2017, and current time is 07:08:26 AM. It will converted become 2017-4-5_7:08:20

        >>> vcpe_id = '123456'
        >>> create_token(vcpe_id)
        '05758f4b0655c06776c0094d1d5c2187'

    """

    #getting current datetime when this function is called
    dt = datetime.now()

    #add zero leading when minutes or seconds is under 10
    if dt.second/10==0:
        second = '00'
    else:
        second = (dt.second/10)*10

    if dt.minute<10:
        minute = '0'+str(dt.minute)
    else:
        minute = str(dt.minute)
    
    #Create combination datetime and vcpe_id
    res = str(dt.year)+'-'+str(dt.month)+'-'+str(dt.day)+'_'+str(dt.hour)+':'+minute+':'+str(second)+'|'+vcpe_id  

    #hashing combination using md5 method  
    token = hashlib.md5(res).hexdigest()

    return token

    #Commented script below for testing and compare token on Server and vCPE
    #req = urllib2.Request('http://188.166.228.168/api/token/')
    #req.add_header('Content-Type', 'application/json')
    #response = urllib2.urlopen(req, json.dumps({'token':token, 'vcpe_id':vcpe_id}))
    #print res
    #print response.read()



def go():
    """Executor and Scheduler Function

    This function to create and start scheduled  config synchronization and statistic sender (calling above function).

    It's can run manually or automatically (using crontab or other startup management apps).

    """

    #Getting vcpe_id from 'vcpe_info' file in 'data' folder
    f = open('../data/vcpe_info', 'r')
    vcpe_id = json.loads(f.read())['vcpe_id']
    f.close()

    """
    Instantiate scheduler library and add job/scheduled function call. It use cron mode (we use cron trigger to enable log printing on terminal when debug the script)
    """
    scheduler = BackgroundScheduler()
    scheduler.add_job(req_config, trigger='cron', minute="*/2", id='req_config', args=[vcpe_id])
    scheduler.add_job(send_stat, trigger='cron', minute="*/15", id='send_stat', args=[vcpe_id])

    #Starting scheduler agent
    scheduler.start()
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try:
        while True:
            time.sleep(2)
    except (KeyboardInterrupt, SystemExit):
        """
        if interupted (manual run via ctrl+c or forcing to close via 'kill ...'), scheduler will shutdown to make sure none proccess is running after this script is stopped
        """
        scheduler.shutdown()

if __name__ == '__main__':
    go()