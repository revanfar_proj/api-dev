from vyos_paramiko import SSH_Helper
import paramiko
from qemu_control import Qemu

class vRouter():
	'''
	This class contain configuration command preparation and execution to vyos via ssh.
	'''

	def __init__(self):
		#self.vyos = vy.vRouter()
		self.commands=''

	def do(self, mode):
		"""
		execution for each configuration command to vyos.

		Attributes:
			mode (str): 'add' to execute add config to vyos. 'del' to execute delete config to vyos

		return:
			str: error message when execute commands via paramiko.
			bool: True, if execution is success
		"""

		#return True
		qe = Qemu()
		if not qe.vnf_status({"name":"vRouter"}):
			return 'vRouter is off, config will applied later'

		log = ''
		cl = self.commands		
		p = 0
		wrp = "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper "

		#Start connection to vyos via ssh paramiko
		vyos = SSH_Helper('192.168.122.1', 'vyos', 'vyos')
		res = vyos.connect()

		#return failed message when failed to connect
		if res == False:
			return 'failed to connect vRouter, configuration aborted'

		#start config session on vyos
		vyos.do(wrp+'begin')

		#splitting commands in self.commands become to list
		if not isinstance(cl, list):
			cl = cl.splitlines()

		#execute each command (call SSH_Helper function do())
		for com in cl:
			p += 1
			#print str(p*100/len(cl))+"%  |",mode,com,"...",

			if mode=='add':
				#execute add config
				print 'sent command:', wrp+'set '+com
				log += vyos.do(wrp+'set '+com)
				print 'result', log
			elif mode=='del':
				#execute delete config
				print 'sent command:', wrp+'delete '+com
				log += vyos.do(wrp+'delete '+com)
				print 'result', log
			print 'success'

		#commit, save to bootfile, then close ssh connection with remote/vyos
		print 'commiting changes...'
		print vyos.do(wrp+'commit')
		vyos.do(wrp+'save')
		vyos.do(wrp+'end')
		vyos.disconnect()
		del vyos

		#set self.commands value to empty
		self.commands = ''

		#return True if no error response from vyos, and return error message if failed to config.
		if len(log)==0:
			return True
		else:
			return log


	def checkAllParam(self, param):
		"""
		function to check one or more parameter value is not empty

		args:
			param (dict): config paramater

		return:
			bool: false if one or more parameter value is empty, true if all parameter is not empty
		"""

		check = True
		for x in param:
			if len(str(param[x]))==0:
				check = False
		if not check:
			print 'some param is null, cancel config process'
		return check


	def isAllParamNull(self, param):
		"""
		function to check all parameter value is empty

		args:
			param (dict): config paramater

		return:
			bool: false if all parameter value is empty, true if all parameter is not empty
		"""
		check = True
		for x in param:
			if len(str(param[x]))>0:
				check = False
		return check


	def vrouter_set_interface(self, param, mode):
		"""
		setting vyos interface (eth2) ip address.

		args:
			param (str): config parameter (ip address)
			mode (str): 'add' to set interface config, 'del' to delete interface config

		return:
			bool: True if param is null (skip config, but reported as success configuration)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#checking parameter is empty or not
		if len(param)==0:
			print 'skipping config, param is null'
			return True

		#configuration command for vyos
		self.commands = '''interfaces ethernet eth2 address %s
''' % (param)
		log = self.do(mode)
		return log


	def vrouter_dhcp(self, param, mode='add'):
		"""
		setting vyos dhcp server

		args:
			param (dict): config parameter
			mode (str): 'add' to set interface config (default mode), 'del' to delete interface config

		return:
			bool: True if param is null (skip config, but reported as success configuration), False if one or more parameter is empty (not all)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#checking all parameter is empty or not
		if self.isAllParamNull(param)==True:
			print 'skipping config, param is null'
			return True

		#checking parameter is empty or not
		if self.checkAllParam(param)!=True:
			return False

		#getting config value from param
		ipnet   = param['ip_subnet']
		def_rou	= param['gateway']
		ipstart = param['ip_start']
		ipstop  = param['ip_stop']
		lease_t	= param['lease']

		#config command for dhcp server setting on vyos
		self.commands = '''service dhcp-server disabled false
service dhcp-server shared-network-name LAN subnet %s default-router %s
service dhcp-server shared-network-name LAN subnet %s lease %s
service dhcp-server shared-network-name LAN subnet %s start %s stop %s
''' % (ipnet, def_rou, 
			ipnet, lease_t,
			ipnet, ipstart, ipstop
			)

		log = self.do(mode)
		return log


	def vrouter_dns_forward(self, param, mode='add'):
		"""
		setting vyos dns forwarding

		args:
			param (dict): config parameter
			mode (str): 'add' to set interface config (default mode), 'del' to delete interface config

		return:
			bool: True if param is null (skip config, but reported as success configuration), False if one or more parameter is empty (not all)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#checking all parameter is empty or not
		if self.isAllParamNull(param)==True:
			print 'skipping config, param is null'
			return True

		#checking parameter is empty or not
		if self.checkAllParam(param)!=True:
			return False

		#getting config parameter value from param
		cch_sz  = 500
		nsrv1   = param['nameserver1']
		nsrv2   = param['nameserver2']

		#config command for dns forwarding on vyos
		self.commands = '''service dns forwarding cache-size %s
service dns forwarding name-server %s
service dns forwarding name-server %s
''' % (cch_sz, nsrv1, nsrv2)
		
		#additional config if mode is add. Prevent error when this command is execute on del mode
		if mode=='add':
			self.commands += '''service dns forwarding listen-on eth2
'''
		log = self.do(mode)
		return log


	def vrouter_rip(self, param, mode='add'):
		"""
		setting vyos rip routing

		args:
			param (dict): config parameter
			mode (str): 'add' to set interface config (default mode), 'del' to delete interface config

		return:
			bool: True if param is null (skip config, but reported as success configuration), False if one or more parameter is empty (not all)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#checking all parameter is empty or not
		if self.isAllParamNull(param)==True:
			print 'skipping config, param is null'
			return True

		#checking parameter is empty or not
		if self.checkAllParam(param)!=True:
			return False

		nw = param['network']

		self.commands = '''protocols rip network %s
''' % (nw)

		log = self.do(mode)
		return log


	def vrouter_ospf(self, param, mode='add'):
		"""
		setting vyos OSPF routing

		args:
			param (dict): config parameter
			mode (str): 'add' to set interface config (default mode), 'del' to delete interface config

		return:
			bool: True if param is null (skip config, but reported as success configuration), False if one or more parameter is empty (not all)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#checking all parameter is empty or not
		if self.isAllParamNull(param)==True:
			print 'skipping config, param is null'
			return True

		#checking parameter is empty or not
		if self.checkAllParam(param)!=True:
			return False

		#getting config parameter value from param
		nw = param['network']
		ar = param['area']

		#add additional command if mode is del or add. Prevent error when execute on vyos
		if mode=='del':
			self.commands = '''policy route-map CONNECT
'''
		else:
			self.commands = '''policy route-map CONNECT rule 10 action permit
policy route-map CONNECT rule 10 match interface log
'''
		
		#command for OSPF routing config for vyos
		self.commands += '''protocols ospf area %s network %s
''' % (ar, nw)

		#add additional command if mode is add
		if mode=='add':
			self.commands +='''
protocols ospf default-information originate always
protocols ospf default-information originate metric 10
protocols ospf default-information originate metric-type 2
protocols ospf log-adjacency-changes
protocols ospf parameters router-id '1.1.1.1'
'''
#protocols ospf redistribute connected metric-type 2
#protocols ospf redistribute connected route-map CONNECT
#'''
		log = self.do(mode)
		return log
		

	def vrouter_static(self, param, mode='add'):		
		"""
		setting vyos static routing

		args:
			param (dict): config parameter
			mode (str): 'add' to set interface config (default mode), 'del' to delete interface config

		return:
			bool: True if param is null (skip config, but reported as success configuration), False if one or more parameter is empty (not all)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#checking all parameter is empty or not
		if self.isAllParamNull(param)==True:
			print 'skipping config, param is null'
			return True

		#checking parameter is empty or not
		if self.checkAllParam(param)!=True:
			return False

		#getting config parameter value from param
		nw = param['network']
		nh = param['nexthop']

		"""
		there are some different command when add and delete static routing config on vyos.
		"""
		if mode=='add':
			self.commands = '''protocols static route %s next-hop %s
''' % (nw, nh)
		elif mode=='del':
			self.commands = '''protocols static route %s
''' % (nw)
		
		log =self.do(mode)
		return log


	def vrouter_src_nat(self, param, mode='add'):
		"""
		setting vyos Source NAT.

		args:
			param (dict): config parameter. but it will ignored, and not used
			mode (str): 'add' to activate source NAT config (default mode), 'del' to deactivate source NAT

		return:
			bool: True if param is null (skip config, but reported as success configuration), False if one or more parameter is empty (not all)
			str: '' (empty string) if config is success, return error message if config failed (error reponse from remote target)
		"""

		#delete/deactivate source NAT
		if mode=='del':
			#shortening delete configuration commands 
			self.commands = '''nat source rule 100
'''
			log =self.do(mode)
			return log

		#add/activate source NAT
		if mode=='add':
			#to standarization and prevent user input wrong source address parameter, it default use 192.168.123.0/29 as parameter virtual network (connected to vFirewall)
			self.commands = '''nat source rule 100 outbound-interface eth2
nat source rule 100 source address 192.168.123.0/29
nat source rule 100 translation address masquerade
'''
		log =self.do(mode)
		return log


"""
	def vrouter_dst_nat(self, param, mode='add'):
		#checking all parameter is empty or not
		if self.isAllParamNull(param)==True:
			print 'skipping config, param is null'
			return True

		#checking parameter is empty or not
		if self.checkAllParam(param)!=True:
			return False
		
		rule = param['rule']

		src_add = param['src_address']
		src_port = param['src_port']

		dst_add = param['dst_address']
		dst_port = param['dst_port']

		tra_add = param['translate_address']
		tra_port = param['translate_port']

		if mode=='del':
			self.commands = '''nat destination rule %s
''' % (rule)
			log =self.do(mode)
			return log

		if mode=='add':
			self.commands = '''nat destination rule %s inbound-interface eth2
nat destination rule %s destination address %s
nat destination rule %s translation address %s
''' % (rule, rule, dst_add, rule, tra_add)

		if len(str(dst_port))>0:
			self.commands += '''nat destination rule %s destination port %s
''' % (rule, dst_port)

		if len(str(src_add))>0:
			self.commands += '''nat destination rule %s source address %s
''' % (rule, src_add)

		if len(str(src_port))>0:
			self.commands += '''nat destination rule %s source port %s
''' % (rule, src_port)

		if len(str(tra_port))>0:
			self.commands += '''nat destination rule %s translation port %s
''' % (rule, tra_port)

		log =self.do(mode)
		return log



----------------------vFirewall---------------------
class vFirewall():
		
	def do(self):
		self.__connect()
		chan = self._ssh.invoke_shell()
		log = ''
		cl = self.commands		
		p = 0

		if not isinstance(cl, list):
			cl = cl.splitlines()
		for com in cl:
			p += 1
			pp = str(p*100/len(cl))+"%"+' | '+com+'...'
			chan.send(com+'\n')
			exit_status = chan.exit_status_ready()
			print exit_status
			if com=='configure':
				time.sleep(3)
			else:
				time.sleep(1)
			output = chan.recv(1024)
			if 'failed' in output:
				print pp,'failed'
			else:
				print pp,'success'
			log += output
		self.__disconnect()
		return log

	def __connect(self):
		_ssh = pm.SSHClient()
		_ssh.set_missing_host_key_policy(pm.AutoAddPolicy())
		_ssh.connect(self.host, self.port, self.usr, self.pwd)
		self._ssh = _ssh

	def __disconnect(self):
		self._ssh.close()
"""