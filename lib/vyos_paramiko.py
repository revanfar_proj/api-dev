import paramiko, json
#import vyos_parser as parser

class SSH_Helper(object):
	"""
	This class for make easier when automate ssh remote when configuring vRouter (vyos). It contain basic ssh remote login, execution, response listener, and logout handler.

	Attributes:
		host (str): remote target ip address
		username (str): remote target username
		password (str): remote target password
		port (int): port for ssh connection
		client (paramiko.SSHClient): SSHClient instantiation
	"""


	def __init__(self, host, username, password, port=22):
		self.host = host
		self.username = username
		self.password = password
		self.port = port
		self.client = False


	def connect(self):
		"""
		initialize and start connection to ssh target.

		Raise:
			'BadHostKeyException': when host ssh key is missing or wrong
			'AuthenticationException': when authentication is failed
			'SSHException': paramiko SSH exception
		"""

		print 'Info:', 'connecting', self.host, '...'
		try:
			self.client = paramiko.SSHClient()

			#if host key is missing, it will add the key automatically
			self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			self.client.load_system_host_keys()

			#start connection to remote target
			self.client.connect(self.host, port=self.port, username=self.username, password=self.password)
			print 'Info:', self.host, 'connected'
			return True
		except (paramiko.BadHostKeyException, paramiko.SSHException, paramiko.AuthenticationException) as e:
			#print 'Warn:', self.host, 'failed to connect'
			print 'Warn', e
			return False
		#print 'ok'


	def do(self, command):
		"""
		helper for command execution to remote target, and listen reponse from execution.
		"""

		#checking if client is connected before
		if self.client==False:
			return 'client is not connected'
		else:
			#execute ssh command and catch response to stdout or stderr
			stdin, stdout, stderr = self.client.exec_command(command)
			err = stderr.read()
			out = stdout.read()
			log = ''
			
			#if there some error catched
			if len(err)>0:
				log += 'error: '+err+'\n'
				print err

			#add response of command execution to temporary log
			if len(out)>0:
				log += 'info: '+out+'\n'
				print out
			return log


	def disconnect(self):

		#checking if client is connected before
		if self.client==False:
			return 'client not connect'
		else:
			print 'Info:', self.host, 'disconnected'

			#close the client, and set value to False
			self.client.close()
			self.client = False
			
			return True


"""
class vRouter(SSH_Parent):
	""docstring for vRouter""
	def __init__(self):
		SSH_Parent.__init__(self, '192.168.122.1', 'vyos', 'vyos')
		self.wrp = "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper "
		self.begin = False
		self.sshres = ''

	def getConfig(self):
		#res = SSH_Parent.connect(self)
		print 'Info:', 'getting config from vRouter'
		if res==True:
			tmp = SSH_Parent.do(self, self.wrp+'show')
			SSH_Parent.disconnect(self)
			sshres = tmp['output'].replace('\\n', '\n')
			par_res = parser.parse_conf(sshres)
			del par_res['interfaces']
			del par_res['system']
			del par_res['service']['ssh']
			return par_res
		else:
			print 'Warn:', 'failed getting config from vRouter'
			print 'cant connect vyos'
			return False

	def config(self):
		print SSH_Parent.connect(self)
		print SSH_Parent.do(self, self.wrp+'begin')		

	def set(self, command):
		print SSH_Parent.do(self, self.wrp+' set '+command)

	def delete(self, command):
		print SSH_Parent.do(self, self.wrp+' delete '+command)

	def commit(self):
		print 'Info:', 'commit and saving vRouter configuration'
		print SSH_Parent.do(self, self.wrp+'commit_with_error')
		print SSH_Parent.do(self, self.wrp+'save')
		print SSH_Parent.do(self, self.wrp+'end')
		print SSH_Parent.disconect(self)
		print 'config end'

'''
vr = vRouter()
if vr==False:
	print 'failed connect'
else:
	#print json.dumps(vr.getConfig(), indent=1, sort_keys=True)
	print vr.config()
	print vr.set('protocols rip network 192.168.76.0/24')
	print vr.commit()
'''"""