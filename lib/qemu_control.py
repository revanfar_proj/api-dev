import libvirt

class Qemu():
	"""
	This class contains helper to monitor qemu vm state and turn it on or off. It's using libvirt library from python-libvirt to monitor and control qemu vm.
	"""

	def __init__(self):
		self.conn = libvirt.open('qemu:///system')


	def __set_dom(self, name):
		#return libvirt domain object when called
		return self.conn.lookupByName(name)


	def vnf_on(self, param):
		"""
		this function to power on VM, and it check current VM state before execute create() function

		args:
			param (dict): must have 'name' key in root.

		return:
			str: 'success' if VM success to power on. 'already on' if VM state previous is already on.
		"""

		name = param['name']
		if self.vnf_status(param):
			return 'already on'
		dom = self.__set_dom(name)
		dom.create()
		#info = dom.info()
		return 'success'

		
	def vnf_off(self, param):
		"""
		this function to shutdown VM, and it check current VM state before execute shutdown() function. By default shutdown function is send shutdown request to vm (safe shutdown)

		args:
			param (dict): must have 'name' key in root. 'force' is optional, and set value to True if want to force shutdown

		return:
			bool: return True, when it's success to shutdown (safely or forced). or return 'already off', if previous 
				  state is off
		"""

		if not self.vnf_status(param):
			return 'already off'
		name = param['name']

		#if 'force' key is on parameter, and it value is True, it will shutdown forcely
		try:
			if param['force']==True:
				self.__set_dom(name).destroy()
				print name,'will shutdown forced'
				return True
		except Exception:
			print name,'will shutdown safely'
		
		self.__set_dom(name).shutdown()
		return True


	def vnf_status(self, param):
		"""
		get current VM state.

		args:
			param (dict): must have 'name' key in root. 'force' is optional, and set value to True if want to force shutdown

		return:
			bool: True if VM is booting, or on. False, if VM is off
		"""

		name = param['name']
		if self.__set_dom(name).isActive()==1:
			return True
		else:
			return False
		

	def __del__(self):
		#close libvirt connection when class child is deleted or destroyed
		self.conn.close()


'''
	def vnf_restart(self, param):

		if not self.vnf_status(param):
			return 'qemu is off'
		name = param['name']
		self.__set_dom(name).destroy()
		self.__set_dom(name).create()
		return True

	def domain_list(self):
		domList = self.conn.listAllDomains(0)
		res = []
		for x in domList:
			res.append(x.name())
		return res

	def __vnf_status_all(self):
		domList = self.__domain_list()
		res = {}
		for x in domList:
			p = {'name' : x}
			res[x] = self.vnf_status(p)
		return res

	def __vnf_status_single(self, name):
		if self.__set_dom(name).isActive()==1:
			return True
		else:
			return False

	def cpu_stat(self, param):
		if not self.vnf_status(param):
			return {'msg':'qemu is inactive'}
		
		name = param['name']
		try:
			interval = param['interval']
		except Exception as e:
			interval = 1
		dom = self.__set_dom(name)
		start = dom.getCPUStats(True)[0]['cpu_time']
		sleep(interval)
		end = dom.getCPUStats(True)[0]['cpu_time']
		return float(end-start)/(interval*1e9)

	def dominfo(self, name):
		return self.__set_dom(name).info()

	def mem_stat(self, param):
		name = param['name']
		res = self.__set_dom(name).info()[2]
		return res

	def strg_stat(self, param):
		name = param['name']
		dom = self.__set_dom(name)
		tree = ElementTree.fromstring(dom.XMLDesc())
		target = tree.find('devices/disk/source').get('file')
		res = os.path.getsize(target)
		return res

	def iface_info(self, param):
		name = param['name']
		res = {}
		if name=='all':
			domList = self.__domain_list()
			for x in domList:
				res[x] = self.__iface_info_single(x)
		else:
			res = self.__iface_info_single(name)
			res['vnf'] = name

		return res

	def __iface_info_single(self, name):
		if not self.vnf_status({'name':name}):
			return {'msg':'qemu is inactive'}
		net = {}

		dom = self.conn.lookupByName(name)
		tree = ElementTree.fromstring(dom.XMLDesc())
		target = tree.find('devices/interface/target').get('dev')

		net['mac'] = tree.find('devices/interface/mac').get('address')
		net['alias'] = tree.find('devices/interface/alias').get('name')

		idx = self.__dhcp_leases(net['mac'])
		if idx == -1:
			return net
		else:
			net.update(idx)
			return net

	def iface_stat(self, param):
		if not self.vnf_status(param):
			return {'msg':'qemu is inactive'}
			
		name = param['name']
		net = {}
		netRead = {}
		netWrite = {}
		dom = self.__set_dom(name)
		tree = ElementTree.fromstring(dom.XMLDesc())
		target = tree.find('devices/interface/target').get('dev')
		net['mac_address'] = tree.find('devices/interface/mac').get('address')
		net['alias'] = tree.find('devices/interface/alias').get('name')
		net['name'] = target
		rwstat = dom.interfaceStats(target)
		netRead['byte'] = rwstat[0]
		netRead['packets'] = rwstat[1]
		netRead['error'] = rwstat[2]
		netRead['drop'] = rwstat[3]
		net['read'] = netRead
		netWrite['byte'] = rwstat[4]
		netWrite['packets'] = rwstat[5]
		netWrite['error'] = rwstat[6]
		netWrite['drops'] = rwstat[7]
		net['write'] = netWrite
		return net

	def __dhcp_leases(self, smac):
		net = self.conn.networkLookupByName('default').DHCPLeases()
		found = -1
		for x in net:
			mac = x['mac']
			if smac==mac:				
				tmp = {}
				tmp['ip'] = x['ipaddr']+'/'+str(x['prefix'])
				tmp['exp'] = x['expirytime']
				found = tmp
		return found

	def __find_mac(self, d, mac):
		ix = 0
		found = -1
		for x in d:
			for ky, y in x.iteritems():
				if y==mac:
					found = ix

			ix+=1
		return found
'''