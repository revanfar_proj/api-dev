import json
from datetime import datetime
from vnf_conf import *
from qemu_control import Qemu

class ConfCheck():
	"""
	This class is for comparing 2 configuration file between conf_example.json and conf_example.json.old to make sure none duplicate configuration that executed to vRouter that can return some error from paramiko.

	Attributes:
		vrouterConf (vRouter) = class instantiation from vnf_conf.vRouter
		qemuControl (Qemu) = class instatiation from qemu_control.Qemu
	"""

	def __init__(self):
		self.vrouterConf = vRouter()
		self.qemuControl = Qemu()

	def readFile(self, _dir):
		"""
		Open config json file then convert it to dictionary

		Args:
			_dir (str) : full path of config directory + file name

		Returns:
			dictionary: if success to read (not empty and success to parsing)

		Raises:
			Parsing error if json format is not valid

		"""

		f = open(_dir, 'r')
		res = f.read()
		f.close()
		return json.loads(res)

	def checkchange(self):
		"""
		this function is for comparing each difference between two configuration dictionary. 
		Every config difference will put to new dictionary called '_set' and '_del' to queue configuration execution to vRouter
		"""

		#getting configuration dictionary from file
		f1 = self.readFile('../data/conf_example.json.old')
		f2 = self.readFile('../data/conf_example.json')
		f1_bak = f1

		#do VNF State checking and comparison with new one
		self.checkVnfState(f2)

		#do WAN Interface checking and comparison between old one and new one
		self.checkWanInterface(f1, f2)

		#Comparing between two src_nat's value from old one and new one. (It's should in new function to make clean code)
		if f1['src_nat'] != f2['src_nat']:
			if f2['src_nat']=='off':
				#if new src_nat is off, it will delete the config
				res = self.execConf('', 'del', 'src_nat')
				if res!=True:
					f2['src_nat'] = f1['src_nat']
			if f2['src_nat']=='on':
				#if new src_nat is on, it will create nat configuration in vRouter
				res = self.execConf('', 'add', 'src_nat')
				if res!=True:
					f2['src_nat'] = f1['src_nat']

		#_set and _del dictionary initiation
		_set = {}
		_del = {}

		#comparing algorithm, if old different->put to _del, if new different->put to _set
		for root in f1:
			if type(f1[root])==dict:
				_set[root] = {}
				_del[root] = {}
				diff = True
				#checking value one-by-one, return true if same, and return false if difference (even just value is difference)
				for sub in f1[root]:
					diff = diff and (False if f1[root][sub]!=f2[root][sub] else True)

				#if difference, put old conf to _del, and new conf to _set
				if not diff:
					_set[root].update(f2[root])
					_del[root].update(f1[root])

			elif type(f1[root])==list:
				"""This algorithm is little complicated because python list is not sorted. To find any added, deleted, or changed config, it must use double looping to find any difference, or little change.
				First loop compare old (as anchor) with new one, to find deleted/changed config.
				Second loop compare new (as anchor) with old one, to find added/changed config
				"""
				_set[root] = []
				_del[root] = []

				#first loop
				for l1 in f1[root]:			
					diff = False
					for l2 in f2[root]:
						for sub in l1:
							#its compare value inner nested dict-array
							diff = diff and (True if l1[sub]==l2[sub] else False)
					if not diff:
						#put to _del if none configuration is same in array (added and changed)
						_del[root].insert(0, l1)

				#second loop
				for l1 in f2[root]:
					diff = False
					for l2 in f1[root]:
						for sub in l2:
							diff = diff and (True if l1[sub]==l2[sub] else False)
					if not diff:
						#put to _set if none configuration is same in array (added and changed)
						_set[root].insert(0, l1)
		
		"""_del and _set dictionary will duplicate all keys (but not with value) from configuration dictionary in root config dictionary.
		Need to delete empty conf parameter to prevent execution with empty parameter.
		"""
		del_key = []
		for x in _set:
			if len(_set[x])==0:
				del_key.insert(0, x)
		for i in del_key:
			del _set[i]

		del_key = []
		for x in _del:
			if len(_del[x])==0:
				del_key.insert(0, x)
		for i in del_key:
			del _del[i]
		
		#for debugging purpose
		#print 'set', _set
		#print 'del', _del

		"""
		Iterate each configuration keys in _del dictionary. If execution is failed, it will added config parameter to f2 (new config dictionary) to keep that config is still in vRouter
		"""
		root_del = []
		for x in _del:
			if type(_del[x])==list:
				del_key = []
				for y in range(len(_del[x])):
					#executing delete config for list type
					res = self.execConf(_del[x][y], 'del', x)
					if res == False:
						#save failed parameter index to del_key list
						del_key.insert(0, y)

				for i in del_key:
					#add failed config parameter to f2
					f2[x].insert(0, _del[x][i])

					#for debug purpose
					del _del[x][i]

			else:
				#executing delete config for dictionary type
				res = self.execConf(_del[x], 'del', x)

				#for debug purpose
				if res==True:
					root_del.insert(0, x)

				if res == False:
					#copy old parameter to new conf dict if failed
					f2[x] = f1[x]


		#for debug purpose
		for i in root_del:
			del _del[i]

		"""
		Iterate each configuration keys in _del dictionary. If execution is failed, it will delete config in f2 (new config dictionary) to prevent wrong info because config not saved to vRouter
		"""
		root_del = []
		for x in _set:
			if type(_set[x])==list:
				del_key = []
				for y in range(len(_set[x])):
					#executing add config for list type
					res = self.execConf(_set[x][y], 'add', x)
					
					if res==False:
						#save failed parameter index to del_key list
						del_key.insert(0, y)						
				for i in del_key:
					#delete new config that failed to execute from f2
					del f2[x][i]

					#for debug purpose
					del _set[x][i]

			else:
				"""executing add config for dict type.
				Will ignore success and failed execution. It all depend on previous config (in delete config execution).
				"""
				res = self.execConf(_set[x], 'add', x)

				#for debug purpose
				if res==True:
					root_del.insert(0, x)

		#for debug purpose
		for i in root_del:
			del _set[i]

		#save success add config and failed delete config to file.
		f = open('../data/conf_example.json.old', 'w')
		f.write(json.dumps(f2, indent=1, sort_keys=True))
		f.close()


	def checkVnfState(self, newConf):
		"""
		Checking current VNF state (on/off) and execute new state from newConf

		Args:
			newConf (dict): full dictionary of new config (downloaded from server). Must have 'vRouter' key and 'on' or 'off' value

		Return:
			str: return 'true' if there are state change and success to change. return error message if there a problem.
		"""

		arr = ['vRouter', 'vFirewall']

		for vnf in arr:
			#getting current state of vRouter
			oldState = 'on' if self.qemuControl.vnf_status({"name":vnf}) else 'off'
			#checking state difference from new config
			if oldState!=newConf[vnf]:
				if newConf[vnf]=='on':
					#execute power on request, and return result message
					print 'power on ', vnf, ' ', str(self.qemuControl.vnf_on({"name":vnf}))
				elif newConf[vnf]=='off':
					#execute power off request, and return result message
					print 'power off ', vnf, ' ', str(self.qemuControl.vnf_off({"name":vnf}))
				else:
					print 'nothing'
		


	def checkWanInterface(self, oldConf, newConf):
		"""
		This function is only to execute WAN Config change.
		It's just comparing between 2 file, not checking real WAN config on vRouter. It can't handle if there some manual change in vRouter (via SSH or tty console)

		Args:
			oldConf (dict): full config dictionary from conf_example.json.old. Must have 'wan_interface' key in there, and valid IP address format in value
			oldConf (dict): full config dictionary from conf_example.json. Must have 'wan_interface' key in there, and valid IP address format in value

		Returns:
			bool: 'true' if successfully to config new WAN Interface and delete old WAN interface
			str: if there some error while configuring, it will return messages
		"""

		oldWan = oldConf['wan_interface']
		newWan = newConf['wan_interface']

		#checking config difference
		if str(oldWan)!=str(newWan):
			print 'changing WAN IP Address from',oldWan,"to",newWan
			log = ''
			
			#execute delete configuration using old wan_interface parameter
			res = self.execConf(oldWan, 'del', 'wan_interface')

			#if configuration failed, it will skip new config and try again later, but still keep old conf in file
			if res != True:
				log += 'Failed to delete old WAN IP Address, cancelling configuration'
			elif res == True:
				#if configuration success, it will execute new configuration.
				res1 = self.execConf(newWan, 'add', 'wan_interface')

				#if failed to execute new configuration, it will config using old config parameter (to keep working config in vRouter)
				if res1 != True:
					log += '\nFailed to config new WAN IP Address, restoring to old one'
					self.execConf(oldWan, 'add', 'wan_interface')	
				else:
					log = True			
			return log


	def execConf(self, param, mode, name):
		"""
		This function is for pairing dictionary key with function name in vnf_conf class.

		Args:
			param (dictionary) : configuration parameter for execution to vRouter
			mode (str) : it can be 'add' (to add config) or 'delete' (to delete config)
			name (str) : filled with dictionary keys from conf dictionary

		Return:
			str: directly return all result from called function
		"""

		if name=="dhcp":
			return self.vrouterConf.vrouter_dhcp(param, mode)
		elif name=="dns":
			return self.vrouterConf.vrouter_dns_forward(param, mode)
		elif name=="rip":
			return self.vrouterConf.vrouter_rip(param, mode)
		elif name=="ospf":
			return self.vrouterConf.vrouter_ospf(param, mode)
		elif name=="static":
			return self.vrouterConf.vrouter_static(param, mode)
		elif name=="src_nat":
			return self.vrouterConf.vrouter_src_nat(param, mode)
		elif name=="dst_nat":
			return self.vrouterConf.vrouter_dst_nat(param, mode)
		elif name=="wan_interface":
			return self.vrouterConf.vrouter_set_interface(param, mode)