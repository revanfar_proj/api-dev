API Middleware for VCPE (v2.alpha)

Perbedaan dari versi 1:
+ Perubahan dari REST API menjadi OID, sehingga ukuran request menjadi lebih kecil
+ OID yang terstruktur, menjadikan pemanggilan fungsi lebih singkat dan aman
+ Manajemen library dan source file yg lebih efisien dan mudah digunakan
+ Struktur JSON yang lebih simpel
json = {
	"oid":"1.1.1",
	"param":{
		"param1":"value1",
		"param2":"value2",
		...
	}
}


~ API Middleware for VCPE (v2.alpha)'s Documentation on progress (waiting for all basic feature is complete)